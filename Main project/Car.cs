﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace H.W_28._10._20
{
    public class Car
    {
        public string Brand { get; private set; }
        public bool TotalLost { get; private set; }
        public bool NeedsRepair { get; internal set; }

        public Car(string brand, bool totalLost, bool needsRepair)
        {
            if (totalLost == true && needsRepair == false)
                throw new RepairMismatchException("total lost car must need repair");
            Brand = brand;
            TotalLost = totalLost;
            NeedsRepair = needsRepair;
        }
    }
}
