﻿using H.W_28._10._20;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GarageTest
{
    public static class TestData
    {
        internal static Car regular_car = new Car("Honda", false, true);
        internal static Car another_car = new Car("Honda", false, true);
        internal static Car total_lost_car = new Car("Yondai", true, true);
        internal static Car unknown_type_car = new Car("Ferari", false, true);
        internal static Car nullCar = null;
        internal static Car mismatchCar = new Car("Honda", false, false);
    }
}
