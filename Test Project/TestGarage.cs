﻿using System;
using System.Collections.Generic;
using H.W_28._10._20;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace GarageTest
{
    [TestClass]
    public class TestGarage
    {
        private Garage garage;
        [TestInitialize]
        public void TestIntialize()
        {
            List<string> CarTypes = new List<string>
            {"Honda", "Yondai", "Suzuki", "Mitzubishi", "Reno" };
            garage = new Garage(CarTypes);
        }
        [TestMethod]
        [ExpectedException (typeof(CarAlreadyHereException))]
        public void Add_Car_exists_exception()
        {
            garage.AddCar(TestData.regular_car);
            garage.AddCar(TestData.regular_car);
        }
        [TestMethod]
        [ExpectedException(typeof(WeDoNotFixTotalLostException))]
        public void Add_Car_Total_Lost_exception()
        {
            garage.AddCar(TestData.total_lost_car);
        }
        [TestMethod]
        [ExpectedException(typeof(WrongGarageException))]
        public void Add_Car_Wrong_Garage_Exception()
        {
            garage.AddCar(TestData.unknown_type_car);
        }
        [TestMethod]
        [ExpectedException(typeof(CarNullException))]
        public void Add_Car_Null_Exception()
        {
            garage.AddCar(TestData.nullCar);
        }
        [TestMethod]
        [ExpectedException(typeof(RepairMismatchException))]
        public void Add_Repair_Mismatch_Exception()
        {
            garage.AddCar(TestData.mismatchCar);
        }
        [TestMethod]
        public void Add_Car_Test()
        {
            garage.AddCar(TestData.regular_car);
            Assert.AreEqual(garage.CarsCount, 1);
        }
        [TestMethod]
        [ExpectedException(typeof(CarNullException))]
        public void Take_Car_Out_null_Exception()
        {
            garage.TakeCarOut(TestData.nullCar);
        }
        [TestMethod]
        [ExpectedException(typeof(CarNotInGarageException))]
        public void Take_Car_Out_Not_In_Garage_Exception()
        {
            garage.AddCar(TestData.regular_car);
            garage.TakeCarOut(TestData.another_car);
        }
        [TestMethod]
        [ExpectedException(typeof(CarNotReadyException))]
        public void Take_Car_Out_Not_Ready_Exception()
        {
            garage.AddCar(TestData.regular_car);
            garage.TakeCarOut(TestData.regular_car);
        }
        [TestMethod]
        public void Take_Out_Car()
        {
            garage.AddCar(TestData.regular_car);
            int carsNum = garage.CarsCount;
            garage.FixCar(TestData.regular_car);
            garage.TakeCarOut(TestData.regular_car);
            Assert.AreEqual(garage.CarsCount, carsNum - 1);
        }
        [TestMethod]
        [ExpectedException(typeof(CarNullException))]
        public void Fix_Car_null_Exception()
        {
            garage.FixCar(TestData.nullCar);
        }
        [TestMethod]
        [ExpectedException(typeof(CarNotInGarageException))]
        public void Fix_Car_Not_In_Garage_Exception()
        {
            garage.AddCar(TestData.regular_car);
            garage.FixCar(TestData.another_car);
        }
        [TestMethod]
        [ExpectedException(typeof(RepairMismatchException))]
        public void Fix_Car_Mismatch_Exception()
        {
            garage.AddCar(TestData.regular_car);
            garage.FixCar(TestData.regular_car);
            garage.FixCar(TestData.regular_car);
        }
        [TestMethod]
        public void Fix_Car()
        {
            garage.AddCar(TestData.regular_car);
            garage.FixCar(TestData.regular_car);
            Assert.AreEqual(TestData.regular_car.NeedsRepair, false);
        }
    }
}
