﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace H.W_28._10._20
{
    public interface IGarage<T>
    {
        void AddCar(Car car);
        void TakeCarOut(Car car);
        void FixCar(Car car);
    }
}
