﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace H.W_28._10._20
{
    public class Garage: IGarage<Car>
    {
        private List<Car> Cars;
        private List<string> carTypes;
        public int CarsCount { get { return Cars.Count; } }

        public Garage(List<string> carTypes)
        {
            this.carTypes = carTypes;
            Cars = new List<Car>();
        }
        public void AddCar(Car car)
        {
            if (car is null)
                throw new CarNullException("Car data wasn't recieved");
            if (Cars.Contains(car))
                throw new CarAlreadyHereException("Car is already in the garage");
            if (car.TotalLost == true)
                throw new WeDoNotFixTotalLostException("Car is total lost and can't be added to the garage");
            bool check = false;
            foreach (string carType in carTypes)
            {
                if (car.Brand == carType)
                {
                    check = true;
                    break;
                }
            }
            if (!check)
                throw new WrongGarageException("We dont add this kind of car to the garage");
            if (car.NeedsRepair == false)
                throw new RepairMismatchException("Car does'nt need repair");
            Cars.Add(car);
        }

        public void FixCar(Car car)
        {
            if (car is null)
                throw new CarNullException("Car data wasn't recieved");
            bool check = false;
            foreach (Car item in Cars)
            {
                if (car == item)
                {
                    check = true;
                    break;
                }
            }
            if (!check)
                throw new CarNotInGarageException("Car wasn't found in garage");
            car.NeedsRepair = false;
        }

        public void TakeCarOut(Car car)
        {
            if (car is null)
                throw new CarNullException("Car data wasn't recieved");
            bool check = false;
            foreach (Car item in Cars)
            {
                if (car == item)
                {
                    check = true;
                    break;
                }
            }
            if (!check)
                throw new CarNotInGarageException("Car wasn't found in garage");
            if (car.NeedsRepair == true)
                throw new CarNotReadyException("Car is stil not ready and need repair");
            Cars.Remove(car);
        }
    }
}
