﻿using System;
using System.Runtime.Serialization;

namespace H.W_28._10._20
{
    [Serializable]
    public class WrongGarageException : Exception
    {
        public WrongGarageException()
        {
        }

        public WrongGarageException(string message) : base(message)
        {
        }

        public WrongGarageException(string message, Exception innerException) : base(message, innerException)
        {
        }

        protected WrongGarageException(SerializationInfo info, StreamingContext context) : base(info, context)
        {
        }
    }
}